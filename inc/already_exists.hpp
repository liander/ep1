//
// Created by liander on 26/09/2019.
//

#ifndef EP1_ALREADY_EXISTS_HPP
#define EP1_ALREADY_EXISTS_HPP
#include <exception>
class already_exists: public std::exception{
private:
    const char * mensagem;
    int indice;
    already_exists();
public:
    already_exists(const char * msg, int index) noexcept: mensagem(msg), indice(index){};
    const char * what();
    int location();
};
#endif //EP1_ALREADY_EXISTS_HPP
