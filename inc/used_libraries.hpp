//
// Created by liander on 22/09/2019.
// ESTE ARQUIVO SERVE COMO REFERÊNCIA BASE PARA OS INCLUDES USADOS NO PROJETO

#ifndef EP1_USED_LIBRARIES_HPP
#define EP1_USED_LIBRARIES_HPP

#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>

#include "not_found.hpp"
#include "already_exists.hpp"

using namespace std;
#endif //EP1_USED_LIBRARIES_HPP
