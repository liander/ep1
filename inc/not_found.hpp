//
// Created by liander on 22/09/2019.
//

#ifndef EP1_NOT_FOUND_HPP
#define EP1_NOT_FOUND_HPP
#include <exception>
class not_found: public std::exception{
private:
    const char * mensagem;
    not_found();
public:
    not_found(const char * msg) noexcept: mensagem(msg){};
    const char * what();
};
#endif //EP1_NOT_FOUND_HPP
