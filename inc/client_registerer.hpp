//
// Created by liander on 22/09/2019.
//

#ifndef EP1_CLIENT_REGISTERER_HPP
#define EP1_CLIENT_REGISTERER_HPP
//Classe do registrador.É responsável por manipular dados
#include "cliente.hpp"
#include "generic_registerer.hpp"
class ClientRegister: public Registerer{
public:

    //FUNÇÕES PRA CLIENTE
    vector<Cliente *> fetch_client_list(); //passa os dados de um arquivo para a lista criada
    vector<Cliente *> register_client(vector<Cliente *> lista);
    vector<Cliente *> remove_client(vector<Cliente *> lista);
    int search_client_index(vector<Cliente *> lista, bool registry);
    int search_client_index(vector<Cliente *> lista, string nome);
    void register_socio(vector<Cliente *> lista, bool valor);
    void save_clients(vector<Cliente *> lista); //salva a atual lista para um arquivo
    void atualiza_dados(vector<Cliente *> lista);
    void register_socio(vector<Cliente *> lista, string nome);

};
#endif //EP1_CLIENT_REGISTERER_HPP
