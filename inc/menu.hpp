//
// Created by liander on 27/09/2019.
//

#ifndef EP1_MENU_HPP
#define EP1_MENU_HPP

#include "used_libraries.hpp"
#include "client_registerer.hpp"
#include "product_register.hpp"

using namespace std;
class Menu{
public:

    void init();
    virtual void display() = 0;
    virtual int get_option() = 0;
    void refresh();
    ~Menu() = default;
    Menu() = default;

//Template para criar novos menus
    template<class C1>
    void createMenu(){
        refresh();
        C1 submenu;
        submenu.init();
    }
//fim do template

//Template para tratamento de dados de Input
    template<typename T1>
    T1 getInput() {
        while(true){
            T1 valor;
            cin >> valor;
            if(cin.fail()){
                cin.clear();
                cin.ignore(32767,'\n');
                cout << "Entrada inválida! Insira novamente: " << endl;
            }
            else{
                cin.ignore(32767,'\n');
                return valor;
            }
        }

    }
    //Final do template
};
#endif //EP1_MENU_HPP
