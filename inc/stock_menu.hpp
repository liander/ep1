//
// Created by liander on 28/09/2019.
//

#ifndef EP1_STOCK_MENU_HPP
#define EP1_STOCK_MENU_HPP

#include "menu.hpp"

class Stock_Menu: public Menu{
private:
    vector<Produto *> estoque;
    ProductRegister master;
public:
    void init();
    void display();
    int get_option();
};
#endif //EP1_STOCK_MENU_HPP
