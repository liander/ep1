//
// Created by liander on 22/09/2019.
//

#ifndef EP1_CLIENTE_HPP
#define EP1_CLIENTE_HPP
#include <string>
using namespace std;
class Cliente{
private:
    string nome;
    string email;
    string cpf; //dadas as dimensões da loja, posso escolher string como o armazenamento do cpf (facilita minha vida)
    string telefone;
    bool is_socio;
    int recent; //é a categoria recém comprada, no caso de compra de múltiplos estoque, o produto em maior quantidade
    //será colocado aqui. O objetivo da variável é o algoritmo de recomendação, que recomendará um produto aleatório
    //da categoria recentemente comprada.
    Cliente();
public:
    Cliente(string nome, string cpf); //registra um cliente qualquer
    Cliente(string nome, string email, string cpf, string telefone);//registra um cliente que quer ser sócio
    Cliente(string nome, string email, string cpf, string telefone, string socio, string categoria);
    //apenas funcionamento interno
    Cliente(Cliente * local_client); //receber um cliente já existente em uma lista, apenas para funcionamento interno

    string get_nome();
    string get_email();
    string get_cpf();
    string get_telefone();
    string verif_socio();
    bool get_socio();
    int get_recent();
    void set_nome(string nome);
    void set_email(string email);
    void set_cpf(string cpf);
    void set_telefone(string telefone);
    void set_socio(bool valor);
    void set_recent(int recent);

    void imprime_dados();
};
#endif //EP1_CLIENTE_HPP
