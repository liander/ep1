//
// Created by liander on 24/09/2019.
//

#ifndef EP1_GENERIC_REGISTERER_HPP
#define EP1_GENERIC_REGISTERER_HPP

#include "used_libraries.hpp"
#include "not_found.hpp"
class Registerer{
public:
    void verify_file(const char * file);
    //Template para receber dados
    template <typename T1>

    T1 getInput(){
        while(true){
            T1 valor;
            cin >> valor;
            if(cin.fail()){
                cin.clear();
                cin.ignore(32767,'\n');
                cout << "Entrada inválida! Insira novamente: " << endl;
            }
            else{
                cin.ignore(32767,'\n');
                return valor;
            }
        }
    }
//Fim do template para receber dados
};
#endif //EP1_GENERIC_REGISTERER_HPP
