//
// Created by liander on 27/09/2019.
//

#ifndef EP1_SALES_MENU_HPP
#define EP1_SALES_MENU_HPP
#include "menu.hpp"

class Sales_Menu: public Menu{
private:
    ClientRegister c_mgr;
    ProductRegister p_mgr;
    Cliente *atual_cliente = nullptr;
    vector<Cliente *> clientes;
    vector<Produto *> estoque;
    vector<Produto *> carrinho;
    float desconto  = 0;
    float valor_total = 0;
public:
    void display();
    int get_option();
    void init();
    void add_to_cart();
    void show_cart();
    bool confirm_sale();
};
#endif //EP1_SALES_MENU_HPP
