//
// Created by liander on 27/09/2019.
//

#ifndef EP1_CLIENT_MANAGEMENT_MENU_HPP
#define EP1_CLIENT_MANAGEMENT_MENU_HPP

#include "menu.hpp"

class C_MGR_Menu: public Menu{
private:
    vector<Cliente *> clientes;
    ClientRegister local;
public:
    int get_option();
    void display();
    void init();

};
#endif //EP1_CLIENT_MANAGEMENT_MENU_HPP
