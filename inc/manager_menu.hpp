//
// Created by liander on 27/09/2019.
//

#ifndef EP1_MANAGER_MENU_HPP
#define EP1_MANAGER_MENU_HPP

#include "menu.hpp"
class ManagerMenu: public Menu{
public:
    void display();
    int get_option();
};
#endif //EP1_MANAGER_MENU_HPP
