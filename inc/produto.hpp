//
// Created by liander on 22/09/2019.
//

#ifndef EP1_PRODUTO_HPP
#define EP1_PRODUTO_HPP
#include <string>
using namespace std;
class Produto {
private:
    string nome;
    float valor;
    int categoria;
    int quantidade;
    string codigo;
    Produto();
public:
    Produto(string nome, string codigo, float valor, int categoria);
    Produto(string nome, string codigo, float valor, int categoria, int quantidade);
    string get_nome();

    float get_valor();

    int get_categoria();

    int get_quantidade();

    string get_codigo();

    void set_nome(string nome);

    void set_valor(float valor);

    void set_categoria(int categoria);

    void set_quantidade(int quantidade);

    void set_codigo(string codigo);
};
#endif //EP1_PRODUTO_HPP
