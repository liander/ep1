//
// Created by liander on 25/09/2019.
//

#ifndef EP1_PRODUCT_REGISTER_HPP
#define EP1_PRODUCT_REGISTER_HPP

#include "generic_registerer.hpp"
#include "produto.hpp"
class ProductRegister: public Registerer{
public:
    //FUNÇÕES PRA PRODUTO
    vector<Produto *> fetch_product_list(); //pega os estoque do arquivo e colocam em lista
    void save_products(vector<Produto *> lista); //salva a lista de estoque ativa para um arquivo estoque.txt
    vector<Produto *> add_product_quantity(vector<Produto *> lista);
    vector<Produto *> register_product(vector<Produto *> lista);
    int search_product_index(vector<Produto *> lista, bool registry);
    void imprime_estoque(vector<Produto *> lista);
};


#endif //EP1_PRODUCT_REGISTER_HPP
