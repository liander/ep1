//
// Created by liander on 23/09/2019.
//
#include "client_registerer.hpp"
#include "not_found.hpp"
#include "already_exists.hpp"

vector<Cliente *> ClientRegister::fetch_client_list(){
    vector<Cliente *> lista;
    fstream arquivo;
    arquivo.open("clientes.txt");
    int cont = 0;
    string nome; string cpf; string email; string telefone; string socio; string categoria;
    while(!arquivo.eof()){
        string line;
        getline(arquivo,line);
        switch(cont%6){
            case 0:
                nome = line;
                break;
            case 1:
                cpf = line;
                break;
            case 2:
                email = line;
                break;
            case 3:
                telefone = line;
                break;
            case 4:
                socio = line;
                break;
            case 5:
                categoria = line;
                lista.push_back(new Cliente(nome, email, cpf, telefone, socio, categoria));
                break;
        }
        cont++;
    }
    arquivo.close();
    return lista;
}

vector<Cliente *> ClientRegister::register_client(vector<Cliente *> lista){
    cout << "Registro de Cliente" << endl << endl;
    try {
        if(lista.size() != 0){
            search_client_index(lista, true); //isso deve retornar exceção caso o cliente já exista na lista
        }else{
            cout << "Não existem clientes registrados" << endl;
        }
        vector<Cliente *> nova_lista = lista;
        cout << "O que deseja fazer?" << endl;
        cout << "(1) Registrar um cliente simples" << endl;
        cout << "(2) Registrar um cliente sócio" << endl;
        int option = getInput<int>();
        string nome;
        string cpf;
        switch (option) {
            case 1:
                system("clear");
                cout << "Registro de cliente" << endl << endl;
                cout << "Insira o nome: ";
                getline(cin, nome);
                cout << "Insira o CPF: ";
                getline(cin, cpf);
                nova_lista.push_back(new Cliente(nome, cpf));
                system("clear");
                cout << "Cliente registrado com sucesso!" << endl;
                break;
            case 2: {
                string email;
                string telefone;
                system("clear");
                cout << "Registro de cliente sócio" << endl << endl;
                cout << "Insira o nome: ";
                getline(cin, nome);
                cout << "Insira o CPF: ";
                getline(cin, cpf);
                cout << "Insira o email: ";
                getline(cin, email);
                cout << "Insira o Telefone: ";
                getline(cin, telefone);
                nova_lista.push_back(new Cliente(nome, email, cpf, telefone));
                system("clear");
                cout << "Cliente registrado com sucesso!" << endl<< endl ;
            }

        }
        return nova_lista;
    }catch(already_exists &e){
        system("clear");
        cout << e.what() << endl << endl;
        return lista;
    }
}

vector<Cliente *> ClientRegister::remove_client(vector<Cliente *> lista){
    cout << "Remover cliente da lista" << endl << endl;
    int indice = search_client_index(lista, false);
    if(indice >=0) {
        vector<Cliente *> nova_lista = lista;
        system("clear");
        cout << "Apagando cliente " + lista[indice]->get_nome() + " da lista de clientes registrados!" << endl << endl;
        lista.erase(lista.begin() + indice);
        return lista;
    }else{
        cout << "Saindo da tela de remoção de clientes!" << endl;
        return lista;
    }
}

int ClientRegister::search_client_index(vector<Cliente *> lista, bool registry) {
    // o registry serve para saber se essa função está sendo usada para fins de registro de NOVO cliente;
    string nome;
    while(true){
        cout << "Digite o nome do cliente: ";
        getline(cin, nome);
        bool tryloop = true;
        try {

            auto iterador = find_if(lista.begin(), lista.end(), [&nome](Cliente client){return client.get_nome() == nome;});
            if ((iterador != lista.end()) && (!registry)){
                return distance(lista.begin(), iterador);
            }else if((iterador != lista.end()) && (registry)){
                throw already_exists("Cliente já registrado!", distance(lista.begin(), iterador));
            }else if(((iterador == lista.end()) && (!registry))){
                throw not_found("Cliente não encontrado! O que deseja fazer?");
            }else if(((iterador == lista.end()) && (registry))) {
                cout << "Cliente não registrado! Favor registrar o cliente";
                cout << endl << endl;
                return -1;
            }
        } catch (not_found &e){
            cout << endl << e.what() << endl;
            while(tryloop){
                cout << "(1) Tentar novamente" << endl << "(0) Sair" << endl;
                switch (getInput<int>()) {
                    case 1:
                        system("clear");
                        tryloop = false;
                        break;
                    case 0:
                        system("clear");
                        return -1; //se optar por sair, o retorno de índice é -1;
                    default:
                        cout << "Entrada inválida! Por favor, digite novamente: " << endl;
                }
            }
        }
    }

}

void ClientRegister::register_socio(vector<Cliente *> lista, bool valor) {
    int indice = search_client_index(lista, false);
    if(indice>=0) {
        lista[indice]->set_socio(valor);
        if(valor) cout << "O(A) cliente "+lista[indice]->get_nome()+" agora é sócio(a)!" << endl << endl;
        if(!valor) cout << "O(A) cliente "+lista[indice]->get_nome()+" não é mais sócio(a)!" << endl << endl;
    }else if(indice == -1){
        cout << "Saindo da tela de registro de sócio!" << endl << endl;
    }
}

void ClientRegister::save_clients(vector<Cliente *> lista){
    fstream arquivo;
    system("rm -r -f clientes.txt");
    system("touch clientes.txt");
    arquivo.open("clientes.txt");
    for(Cliente * c: lista){
        arquivo << c->get_nome() << endl << c->get_cpf() << endl << c->get_email() << endl;
        arquivo << c->get_telefone() << endl << c->verif_socio() << endl << c->get_recent() << endl;
    }
    cout << "A atual lista de clientes foi salva com sucesso!" << endl;
    arquivo.close();
}

void ClientRegister::atualiza_dados(vector<Cliente *> lista) {
    cout << "Atualizar/Corrigir dados de cliente" << endl << endl;
    int indice;
    indice = search_client_index(lista, false);

    if(indice >= 0){
        string nome; string cpf; string email; string telefone;

        cout << "Dados atuais do Cliente:" << endl << endl;
        lista[indice]->imprime_dados();
        cout << endl << "Insira os novos dados do cliente " << endl << endl;

        cout << "Insira o nome: ";
        getline(cin, nome);
        lista[indice]->set_nome(nome);
        cout << "Insira o CPF: ";
        getline(cin, cpf);
        lista[indice]->set_cpf(cpf);
        cout << "Insira o email: ";
        getline(cin, email);
        lista[indice]->set_email(email);
        cout << "Insira o Telefone: ";
        getline(cin, telefone);
        lista[indice]->set_telefone(telefone);

        system("clear");
        cout << endl << "Novos dados do cliente: " << endl << endl;
        lista[indice]->imprime_dados();
        cout << endl;
    }else{
        cout << "Saindo da tela de correção de dados!" << endl;
    }
}

void ClientRegister::register_socio(vector<Cliente *> lista, string nome){
    int indice = search_client_index(lista, nome);
    lista[indice]->set_socio(true);
    cout << "O(A) cliente "+lista[indice]->get_nome()+" agora é sócio(a)!" << endl << endl;

}//sobrecarga para o caso de registrar um sócio quando já se tem o nome

int ClientRegister::search_client_index(vector<Cliente *> lista, string nome) {
    auto iterador = find_if(lista.begin(), lista.end(), [&nome](Cliente client){return client.get_nome() == nome;});
    return distance(lista.begin(), iterador);
}//sobrecarga pra retornar o indice de um cliente quando já se tem uma variável disponível com o nome

