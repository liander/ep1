//
// Created by liander on 27/09/2019.
//
#include "client_management_menu.hpp"
void C_MGR_Menu::display() {
    cout << "Bem vindo ao menu de Gerenciamento de Clientes" << endl;
    cout << "(1) Registrar novo cliente" << endl;
    cout << "(2) Tornar sócio um cliente" << endl;
    cout << "(3) Remover status de sócio de um cliente" << endl;
    cout << "(4) Corrigir os dados de um cliente" << endl;
    cout << "(5) Remover um cliente do registro" << endl;
    cout << "(6) Imprimir registro de clientes" << endl;
    cout << endl << "(0) Sair" << endl;
    cout << "-> ";
}

int C_MGR_Menu::get_option() {
    int option = getInput<int>();
    switch(option){
        case 1:
            refresh();
            clientes = local.register_client(clientes);
            break;
        case 2:
            refresh();
            cout << "Registro de Sócio" << endl << endl;
            local.register_socio(clientes, true);
            break;
        case 3:
            refresh();
            cout << "Remover Sócio" << endl << endl;
            local.register_socio(clientes, false);
            break;
        case 4:
            refresh();
            local.atualiza_dados(clientes);
            break;
        case 5:
            refresh();
            clientes = local.remove_client(clientes);
            break;
        case 6:
            refresh();
            cout << "Atual lista de clientes" << endl;
            for(Cliente * c: clientes) c->imprime_dados();
            cout << endl << endl;
            break;
        case 0:
            refresh();
            local.save_clients(clientes);
            return false;
        default:
            cout << "Opção inválida! Digite novamente: ";
    }
    return true;
}

void C_MGR_Menu::init() {
    clientes = local.fetch_client_list();
    Menu::init();
}
