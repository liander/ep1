//
// Created by liander on 25/09/2019.
//

#include "product_register.hpp"
#include "already_exists.hpp"
#include "not_found.hpp"
int ProductRegister::search_product_index(vector<Produto *> lista, bool registry) {
    string codigo;
    while(true){
        cout << "Digite o código do produto: ";
        getline(cin, codigo);
        bool tryloop = true;
        try {
            auto iterador = find_if(lista.begin(), lista.end(), [&codigo](Produto *product){return product->get_codigo() == codigo;});
            if ((iterador != lista.end()) && (!registry)){
                return std::distance(lista.begin(), iterador);
            }else if((iterador != lista.end()) && (registry)){
                throw already_exists("O produto já consta no estoque!", std::distance(lista.begin(), iterador));
            }else if(((iterador == lista.end()) && (!registry))){
                throw not_found("Produto não encontrado! O que deseja fazer?");
            }else if(((iterador == lista.end()) && (registry))) {
                cout << "O produto ainda não foi registrado!";
                cout << endl << endl;
                return 0;
            }
        } catch (not_found &e){
            cout << endl << e.what() << endl;
            while(tryloop){
                cout << "(1) Tentar novamente" << endl << "(2) Imprimir estoque atual e tentar novamente" << endl;
                cout << "(0) Sair" << endl;
                cout << "-> ";
                switch (getInput<int>()) {
                    case 1:
                        system("clear");
                        tryloop = false;
                        break;
                    case 2:
                        cout << endl;
                        system("clear");
                        imprime_estoque(lista);
                        cout << endl;
                        tryloop = false;
                        break;
                    case 0:
                        return -1; //se optar por sair, o retorno de índice é -1;
                    default:
                        cout << "Entrada inválida! Por favor, digite novamente: " << endl;
                }
            }
        }
    }
}

vector<Produto *> ProductRegister::register_product(vector<Produto *> lista) {
    try {
        if(lista.size() != 0){
            search_product_index(lista, true); //isso deve retornar exceção caso o produto já exista na lista
        }else{
            cout << "Não existem produtos registrados" << endl << endl;
        }
        vector<Produto *> nova_lista = lista;
        cout << "O que deseja fazer?" << endl;
        cout << "(1) Registrar um produto sem declarar quantidade" << endl;
        cout << "(2) Registrar um produto e sua quantidade" << endl;
        cout << endl << "(0) Sair da tela de Registro de Produto" << endl;
        int option = getInput<int>();
        string nome;
        string codigo;
        float valor;
        int categoria;
        switch (option) {
            case 1:
                system("clear");
                cout << "Registro de novo produto" << endl << endl;
                cout << "Insira o nome: ";
                getline(cin, nome);
                cout << "Insira o código de produto: ";
                getline(cin, codigo);
                cout << "Insira o valor do produto (por unidade): ";
                valor = getInput<float>();
                cout << "Insira a categoria do produto: ";
                categoria = getInput<int>();
                nova_lista.push_back(new Produto(nome, codigo, valor, categoria));
                break;
            case 2: {
                system("clear");
                cout << "Registro de novo produto" << endl << endl;
                int quantidade;
                cout << "Insira o nome: ";
                getline(cin, nome);
                cout << "Insira o código de produto: ";
                getline(cin, codigo);
                cout << "Insira o valor do produto (por unidade): ";
                valor = getInput<float>();
                cout << "Insira a categoria do produto: ";
                categoria = getInput<int>();
                cout << "Insira a quantidade do produto: ";
                quantidade = getInput<int>();
                nova_lista.push_back(new Produto(nome, codigo, valor, categoria, quantidade));
                break;
            }
            case 0:
                system("clear");
                cout << "Saindo da tela de Registro de Produto!" << endl << endl;
                return lista;
            default :
                cout << "Opção incorreta! Digite novamente: " << endl;
                break;

        }
        return nova_lista;
    }catch(already_exists &e){
        cout << e.what() << endl;
        return lista;
    }
}

void ProductRegister::imprime_estoque(vector<Produto *> lista) {
cout << setw(30) << left << "Código" << setw(30) << "Nome"<< setw(30) << "Valor (por unidade)" << setw(30);
cout << "Quantidade disponível:" << setw(30) << "Categoria" << endl;
    for(Produto * p: lista){
        cout << setw(30) << left << p->get_codigo() << setw(30) << p->get_nome() << setw(30);
        cout << p->get_valor() << setw(30) << p->get_quantidade() << setw(30) << p->get_categoria() << endl;
    }
}

vector<Produto *> ProductRegister::add_product_quantity(vector<Produto *> lista) {
    int indice = search_product_index(lista, false);
    if(indice>=0) {
        cout << "Insira a quantidade para adicionar ao produto: ";
        int quantidade = getInput<int>();
        lista[indice]->set_quantidade(lista[indice]->get_quantidade()+quantidade);
        system("clear");
        cout << to_string(quantidade)+" unidades adicionadas ao estoque de "+lista[indice]->get_nome()+"!" << endl << endl;
    }else{
        system("clear");
        cout << "Saindo da tela de mudança de estoque!" << endl << endl;
    }
    return lista;
}

vector<Produto *> ProductRegister::fetch_product_list(){
    vector<Produto *> lista;
    fstream arquivo;
    arquivo.open("estoque.txt");
    int cont = 0;
    string nome; string codigo; string valor; string categoria; string quantidade;
    while(!arquivo.eof()){
        string line;
        getline(arquivo,line);
        switch(cont%5){
            case 0:
                nome = line;
                break;
            case 1:
                codigo = line;
                break;
            case 2:
                valor = line;
                break;
            case 3:
                categoria = line;
                break;
            case 4:
                quantidade = line;
                lista.push_back(new Produto(nome, codigo, stof(valor), stoi(categoria), stoi(quantidade)));
                break;
        }
        cont++;
    }
    arquivo.close();
    return lista;
}

void ProductRegister::save_products(vector<Produto *> lista){
        fstream arquivo;
        system("rm -r -f estoque.txt");
        system("touch estoque.txt");
        arquivo.open("estoque.txt");
        for(Produto * p: lista){
            arquivo << p->get_nome() << endl << p->get_codigo() << endl << p->get_valor() << endl;
            arquivo << p->get_categoria() << endl << p->get_quantidade() << endl;
        }
        cout << "Estoque salvo com sucesso!" << endl;
        arquivo.close();
    }



