//
// Created by liander on 22/09/2019.
//

#include "produto.hpp"
string Produto::get_nome(){
    return nome;
}
float Produto::get_valor() {
    return valor;
}

int Produto::get_categoria() {
    return categoria;
}

int Produto::get_quantidade() {
    return quantidade;
}

string Produto::get_codigo() {
    return codigo;
}

void Produto::set_nome(string nome) {
    this->nome = nome;
}

void Produto::set_valor(float valor) {
    this->valor = valor;
}

void Produto::set_categoria(int categoria) {
    this->categoria = categoria;
}

void Produto::set_quantidade(int quantidade) {
    this->quantidade = quantidade;
}

void Produto::set_codigo(string codigo) {
    this->codigo = codigo;
}

Produto::Produto(string nome, string codigo, float valor, int categoria) {
    set_nome(nome);
    set_codigo(codigo);
    set_valor(valor);
    set_categoria(categoria);
    set_quantidade(0);
}

Produto::Produto(string nome, string codigo, float valor, int categoria, int quantidade) {
    set_nome(nome);
    set_codigo(codigo);
    set_valor(valor);
    set_categoria(categoria);
    set_quantidade(quantidade);
}

