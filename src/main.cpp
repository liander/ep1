//
// Created by liander on 23/09/2019.
//
#include "client_registerer.hpp"
#include "product_register.hpp"
#include "manager_menu.hpp"
using namespace std;
int main() {
    ClientRegister mainRegisterer;
    mainRegisterer.verify_file("clientes.txt");
    mainRegisterer.verify_file("estoque.txt");
    system("clear");
    ManagerMenu master;
    master.init();
}