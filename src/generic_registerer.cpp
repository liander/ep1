//
// Created by liander on 24/09/2019.
//

#include "generic_registerer.hpp"
#include "used_libraries.hpp"
void Registerer::verify_file(const char * file) {
    const char * err_msg = "Arquivo essencial não encontrado! ";
    fstream arquivo;
    arquivo.open(file);
    try {
        if (!arquivo) {
            throw not_found(err_msg);
        }
        cout << "O arquivo '" + string(file) + "' foi verificado com sucesso!" << endl;
        arquivo.close();
    }catch(not_found & e) {
        arquivo.close();
        cout << e.what() << "O arquivo '"+string(file)+"' será criado antes de salvar os registros!" << endl;
        string command = "touch "+string(file);
        system(command.c_str());
    }

}