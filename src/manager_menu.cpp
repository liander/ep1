//
// Created by liander on 27/09/2019.
//
#include "manager_menu.hpp"
#include "sales_menu.hpp"
#include "client_management_menu.hpp"
#include "stock_menu.hpp"
ProductRegister p_mgr;
ClientRegister c_mgr;

void ManagerMenu::display(){
    cout << endl << "Bem vindo ao menu do Gerente:" << endl;
    cout << "(1) Iniciar o modo Venda" << endl;
    cout << "(2) Iniciar o modo Estoque" << endl;
    cout << "(3) Iniciar o modo Recomendação" << endl;
    cout << "(4) Gerenciamento de Clientes" << endl;
    cout << endl << "(0) Sair do programa" << endl;
    cout << "-> ";
}

int ManagerMenu::get_option(){
    int option = getInput<int>();
    switch(option){
        case 1:
            createMenu<Sales_Menu>();
            break;
        case 2:
            createMenu<Stock_Menu>();
            break;
        case 3: {
            refresh();
            p_mgr.verify_file("estoque.txt");
            c_mgr.verify_file("clientes.txt");
            vector<Cliente *> clientes = c_mgr.fetch_client_list();
            int indice = c_mgr.search_client_index(clientes, false);
            if (indice >= 0) {
                Cliente cliente_atual = clientes[indice];
                vector<Produto *> estoque = p_mgr.fetch_product_list();
                vector<Produto *> produtos_selecionados;
                for (Produto *p: estoque) {
                    if (p->get_categoria() == cliente_atual.get_recent()) produtos_selecionados.push_back(p);
                }
                if (!produtos_selecionados.empty()) {
                    system("clear");
                    Produto *recomendado = produtos_selecionados[rand() % produtos_selecionados.size()];
                    cout << "Para o cliente " << cliente_atual.get_nome() << " é recomendado o produto "
                            << recomendado->get_nome() << " com código " << recomendado->get_codigo() +
                            " localizado na categoria "<< recomendado->get_categoria() << endl << endl;
                } else {
                    system("clear");
                    cout << "Aparentemente não existem produtos a serem recomendados para o cliente "
                    << cliente_atual.get_nome() << endl << endl;
                }
            }
            break;
        }
        case 4:
            createMenu<C_MGR_Menu>();
            break;
        case 0:
            refresh();
            return false;
        default:
            cout << "Opção inválida! Digite novamente: " << endl;
    }
    return true;
}
