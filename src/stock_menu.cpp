//
// Created by liander on 28/09/2019.
//

#include "stock_menu.hpp"

void Stock_Menu::init(){
    refresh();
    master.verify_file("estoque.txt");
    estoque = master.fetch_product_list();
    bool open = true;
    if(estoque.empty()) cout<< "Nenhum produto foi registrado até o momento" << endl << endl;
    while(open){
        display();
        open = get_option();
    }
}

void Stock_Menu::display() {
    cout << endl << "Bem vindo ao Gerenciamento de Estoque" << endl;
    cout << "(1) Registrar novo produto" << endl;
    cout << "(2) Acrescentar a quantidade de um produto" << endl;
    cout << "(3) Imprimir estoque atual" << endl;
    cout << endl << "(0) Sair da tela de estoque" << endl;
    cout << "-> ";
}

int Stock_Menu::get_option() {
    switch(getInput<int>()){
        case 1:
            refresh();
            cout << "Registro de novo produto" << endl << endl;
            estoque = master.register_product(estoque);
            cout << endl;
            master.save_products(estoque);
            estoque = master.fetch_product_list(); //Atualiza a atual lista de produtos
            cout << endl;
            break;
        case 2:
            refresh();
            cout << "Acréscimo de quantidade em produto" << endl << endl;
            master.add_product_quantity(estoque);
            break;
        case 3:
            refresh();
            if(!estoque.empty()) {
                cout << "Atual estoque da loja" << endl << endl;
                master.imprime_estoque(estoque);
                cout << endl;
            }else{cout << "O estoque está vazio" << endl << endl;}
            break;
        case 0:
            refresh();
            master.save_products(estoque);
            cout << endl;
            return false;
        default:
            cout << "Opção inválida! Digite novamente" <<endl<<endl;
    }
    return true;
}