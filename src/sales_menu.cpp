//
// Created by liander on 27/09/2019.
//

#include "sales_menu.hpp"
#include "client_registerer.hpp"

void Sales_Menu::init() {

    c_mgr.verify_file("clientes.txt");
    p_mgr.verify_file("estoque.txt");
    clientes = c_mgr.fetch_client_list();
    estoque = p_mgr.fetch_product_list();
    bool good_to_go = false;
    if (!clientes.empty()) {
        try {
            cout << endl;
            c_mgr.search_client_index(clientes, true);
        }
        catch (already_exists &e) {
            int index = e.location();
            atual_cliente = new Cliente(clientes[index]);
            if(atual_cliente->get_socio()) desconto = 0.15; //15%
            good_to_go = true;
        }

        if (good_to_go) {
            bool open = true;
            while (open) {
                cout <<endl<< "Cliente atual: " << atual_cliente->get_nome() << endl;
                if(atual_cliente->get_socio()) desconto = 0.15; //15%
                cout << "É sócio? " << atual_cliente->verif_socio() << endl<<endl;
                display();
                open = get_option();
            }
        }
    }else{
        cout << "Não existem clientes registrados! ";
        cout << "Por favor, acesse o menu de Gerenciamento de Clientes para registrar um cliente" << endl << endl;
    }
}

void Sales_Menu::display() {
    cout << "Bem vindo ao menu de vendas:" << endl;
    cout << "(1) Adicionar item ao carrinho" << endl;
    cout << "(2) Mostrar carrinho" << endl;
    cout << "(3) Confirmar compra" << endl;
    cout << endl << "(0) Cancelar venda" << endl;
}

int Sales_Menu::get_option() {
    switch(getInput<int>()){
        case 1:
            refresh();
            cout << "Adicionar produto ao carrinho" << endl << endl;
            add_to_cart();
            break;
        case 2:
            refresh();
            if(!carrinho.empty()) {
                show_cart();
            }else{cout << "O carrinho está vazio" << endl << endl;}
            break;
        case 3:
            refresh();
            if(!carrinho.empty()) {
                return confirm_sale();
            }else{
                cout << "O carrinho está vazio, não é possível finalizar a compra" << endl << endl;
                break;
            }
        case 0:
            refresh();
            cout << "Saindo da tela de venda" << endl << endl;
            return false;
    }
    return true;
}

void Sales_Menu::add_to_cart() {
    int index = p_mgr.search_product_index(estoque, false);
    if(index>=0) {
        cout << "Insira a quantidade a ser comprada: ";
        int quantidade = getInput<int>();
        if (quantidade < 1){
            refresh();
            cout << "Impossível adicionar essa quantidade de produto" << endl << endl;
        } else {
            bool exists = false;

            Produto *produto_existente = new Produto("", "", 0, 0);
            for (Produto *p: carrinho) {
                if (p->get_codigo() == estoque[index]->get_codigo()) {
                    exists = true;
                    produto_existente = p;
                    break;
                }
            }

            if ((quantidade > estoque[index]->get_quantidade() && !exists) ||
                (quantidade + produto_existente->get_quantidade() > estoque[index]->get_quantidade())) {
                refresh();
                cout << "Impossível adicionar essa quantidade ao carrinho!" << endl;
                cout << "Restam apenas " << estoque[index]->get_quantidade()
                     << " unidades do produto " + estoque[index]->get_nome() << endl;
            } else if (exists) {
                refresh();
                cout << "Adicionando mais " << quantidade
                     << " unidades ao produto " + estoque[index]->get_nome() + " no carrinho!" << endl << endl;
                produto_existente->set_quantidade(produto_existente->get_quantidade() + quantidade);
            } else {
                refresh();
                cout << "Adicionando produto " + estoque[index]->get_nome() + " ao carrinho de compras!" << endl;
                carrinho.push_back(new Produto(estoque[index]->get_nome(), estoque[index]->get_codigo(),
                                               estoque[index]->get_valor(), estoque[index]->get_categoria(),
                                               quantidade));
            }
        }
    }
}

void Sales_Menu::show_cart() {
    cout << endl << "Carrinho do cliente "+atual_cliente->get_nome() << endl << endl;
    cout << setw(30) << left << "Código" << setw(30) << "Nome"<< setw(30) << "Valor (por unidade)"<< setw(30);
    cout << "Quantidade" << setw(30) << "Categoria" << endl;
    for(Produto * p: carrinho){
        cout << setw(30) << left << p->get_codigo() << setw(30) << p->get_nome() << setw(30);
        cout << p->get_valor() << setw(30) << p->get_quantidade() << setw(30) << p->get_categoria() << endl;
    }

    for(Produto * p: carrinho){
        if (valor_total!=0) valor_total = 0;
        valor_total += (p->get_valor()) * (p->get_quantidade());
    }
    if(desconto > 0) {
        float valor_descontado = valor_total - valor_total*(1-desconto);
        valor_total = valor_total-valor_descontado; //valor da compra
        cout << endl << "Com 15% de desconto para sócios, o cliente economizou:  R$ " << valor_descontado << endl;
        cout << "Valor total da compra:  R$ " << valor_total << endl;
    }else {
        cout << endl << "Cliente não possui desconto de sócio! Deseja cadastrá-lo como sócio? " << endl << "(1) Sim" << endl << "(2) Não" << endl;
        switch(getInput<int>()){
            case 1:
                refresh();
                c_mgr.register_socio(clientes, atual_cliente->get_nome());
                atual_cliente->set_socio(true);
                c_mgr.save_clients(clientes);
                cout << endl << "O desconto aparecerá na proxima vez que a opção 'Mostrar o carrinho' for selecionada" << endl;
                break;
            case 2:
                cout << endl << "Valor total da compra:  R$ " << valor_total << endl;
                break;
        }
    }
    cout << endl;
}

bool Sales_Menu::confirm_sale() {
    refresh();
    show_cart();
    Produto produto_mais_comprado("", "", 0,-1, 0);
    while(true) {
        cout << "Tem certeza de que quer confirmar a compra?" << endl << "(1) Sim" << endl << "(2) Não" << endl;
        switch (getInput<int>()){
            case 1: {
                for (Produto *comprado: carrinho) {
                    for (Produto *p: estoque) {
                        if (comprado->get_codigo() == p->get_codigo()) {
                            if (comprado->get_quantidade() > produto_mais_comprado.get_quantidade()) {
                                produto_mais_comprado.set_categoria(comprado->get_categoria());
                                produto_mais_comprado.set_quantidade(comprado->get_quantidade());
                            }
                            p->set_quantidade(p->get_quantidade() - comprado->get_quantidade());
                        }
                    }
                }
                int index = c_mgr.search_client_index(clientes, atual_cliente->get_nome());
                clientes[index]->set_recent(produto_mais_comprado.get_categoria());
                refresh();
                cout << "Compra feita com sucesso!!" << endl << "Cliente: " << atual_cliente->get_nome() << endl
                     << "Valor recebido:  R$ " << valor_total << endl;
                p_mgr.save_products(estoque);
                c_mgr.save_clients(clientes);
                return false;
            }
            case 2:
                refresh();
                return true;
            default:
                cout << "Opção errada, insira novamente: " << endl;
        }
    }
}
