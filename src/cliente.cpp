//
// Created by liander on 22/09/2019.
//

#include "cliente.hpp"
#include <iostream>

using namespace std;
Cliente::Cliente(string nome, string cpf){
    set_nome(nome);
    set_cpf(cpf);
    set_email("Email não fornecido");
    set_telefone("Telefone não fornecido");
    set_socio(false);
    set_recent(0);
}//Construtor para clientes que apenas querem comprar

Cliente::Cliente(string nome, string email, string cpf, string telefone){
    set_nome(nome);
    set_email(email);
    set_cpf(cpf);
    set_telefone(telefone);
    set_socio(true);
}//Construtor para clientes que querem ser sócios

Cliente::Cliente(string nome, string email, string cpf, string telefone, string socio, string categoria) {
    set_nome(nome);
    set_email(email);
    set_cpf(cpf);
    set_telefone(telefone);
    if((socio=="Sim") | (socio=="sim")){
        set_socio(true);
    }else if((socio=="Não") || (socio == "Nao") || (socio== "não") || (socio=="nao")){
        set_socio(false);
    }
    set_recent(stoi(categoria));
} // Construtor usado apenas para recuperar os dados de uma lista salva em arquivo;

void Cliente::set_nome(string nome) {
    this->nome = nome;
}

void Cliente::set_email(string email) {
    this->email = email;
}
void Cliente::set_cpf(string cpf) {
    this->cpf = cpf;
}
void Cliente::set_telefone(string telefone) {
    this->telefone = telefone;
}
void Cliente::set_socio(bool valor) {
    this->is_socio = valor;
}
void Cliente::set_recent(int recent) {
    this->recent = recent;
}

string Cliente::get_nome() {
    return nome;
}
string Cliente::get_email() {
    return email;
}
string Cliente::get_cpf() {
    return cpf;
}
string Cliente::get_telefone() {
    return telefone;
}
string Cliente::verif_socio() {
    switch(is_socio){
        case true:
            return "Sim";
        case false:
            return "Não";
    }
}
bool Cliente::get_socio() {
    return is_socio;
}

int Cliente::get_recent() {
    return recent;
}

void Cliente::imprime_dados() {
    cout << "------------------------------------" << endl;
    cout << "   NOME: " << get_nome() << endl;
    cout << "   CPF: " << get_cpf() << endl;
    cout << "   EMAIL: " << get_email() << endl;
    cout << "   TELEFONE: " << get_telefone() << endl;
    cout << "   É SÓCIO? " << verif_socio() << endl;
    cout << "   Categoria mais comprada: " << get_recent() << endl;
}

Cliente::Cliente(Cliente * local_client) {
    set_nome(local_client->get_nome());
    set_cpf(local_client->get_cpf());
    set_email(local_client->get_email());
    set_telefone(local_client->get_telefone());
    set_socio(local_client->get_socio());
    set_recent(local_client->get_recent());
}
