# Programa para o gerenciamento de uma loja

O programa consiste em uma série de menus para facilitar o gerenciamento de estoque e clientes na loja.

## Funcionamento

As funcionalidades foram separadas em 4 categorias:
- Gerenciamento de Clientes
- Gerenciamento de Estoque
- Modo Venda
- Recomendação

Para operar cada um desses menus, é apenas necessário digitar uma das opções dadas. Algumas partes podem não manter a tela de forma esteticamente agradável mas, com pequenas modificações no código do menu em questão, é possível customizar as mensagens e sua disposição na tela.

O programa é compatível apenas com sistemas semelhantes ao Ubuntu Linux, pois faz uso de funções nativas do mesmo (comando clear). No código estão sendo usadas as bibliotecas nativas do C++ (vector, string, iostream, fstream, cstdlib).
Para compilar e rodar o programa, é necessário estar com o terminal aberto na pasta onde se encontram os arquivos:
- src/
- inc/
- bin/
- obj/
- Makefile

E executar os seguintes comandos:

- **make clean** (caso seja a primeira vez)
- **make**
- **make run**

Quando o programa é executado pela primeira vez, ele cria no diretório do Makefile dois arquivos:
- clientes.txt
- estoque.txt

Estes arquivos serão utilizados para armazenar os cadastros de clientes e de produtos. É recomendado que estes arquivos jamais sejam modificados por meios que não sejam o programa.

Após isso, aparecerão em seu terminal as opções principais (recomendado que o terminal esteja em tela cheia). Segue a descrição de cada uma:

- **Modo Venda:** esta opção leva ao menu responsável por realizar vendas. Antes de iniciar a venda será pedido o nome do cliente e, caso o mesmo não existir, é necessário que este seja registrado através do menu **Gerenciamento de Clientes**

- **Modo Estoque:** este menu conta com opções de registro de novos produtos, acréscimo da quantidade de produtos existentes e impressão do estoque atualmente registrado
  - Os produtos estão separados por categorias numéricas, é necessário que o usuário separe corretamente os produtos em categorias. No caso de categorias mistas, pode-se criar uma nova categoria (um número novo) que represente duas categorias ao mesmo tempo. Isto é feito no momento do registro da categoria de um produto.
  - O registro do código de produtos está sendo feito no formato XXXYYYY, sendo XXX a categoria do produto (admite até 999 tipos diferentes) e YYYY o número do produto (admite até 9999 produtos diferentes). Caso necessário, é possível apenas adotar um padrão diferente.
- **Modo Recomendação:** esta opção pede o nome de um cliente e, baseado na categoria mais comprada por ele(a), o programa recomendará um produto aleatório pertencente à categoria em questão.
- **Gerenciamento de Clientes:** esta opção permite registrar, remover, associar, desassociar e corrigir os dados cadastrais de clientes além de poder mostrar o registro de todos os clientes cadastrados atualmente.
